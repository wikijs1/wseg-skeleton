import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Product } from '../models/product.model';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
//Service für Warenkorb
export class CartService {

  private dbPath = '/products';

  productsRef: AngularFirestoreCollection<Product>;

  //Liste von Produkten im Warenkorb
  public cartItemList : Product[] = []
  public productList = new BehaviorSubject<any>([]);
  public search = new BehaviorSubject<string>("");

  constructor(private db: AngularFirestore) { this.productsRef = db.collection(this.dbPath); }

  getProducts(){
    return this.productList; 
    // return this.productList.asObservable();
  }

  setProduct(product : any){
    this.cartItemList.push(...product);
    this.productList.next(product);
  }
  //Fügt Produkt dem Warenkorb hinzu
  addtoCart(product : any){
    this.getTotalPrice();

    this.cartItemList.forEach(value => {
      if (value.id == product.id) {
        product.quantity++;
        return 
      }
    })
    this.cartItemList.push(product);
    this.productList.next(this.cartItemList);
  }
  //Preis ausrechneen
  getTotalPrice(): number{
    let grandTotal = 0;
    this.cartItemList.map((a:Product)=>{
      grandTotal += Number(a.productPrice);
    })
    return grandTotal;
  }
  //Produkt vom Warenkorb entfernen
  removeCartItem(product: any){
    this.cartItemList.map((a:any, index:any)=>{
      if(product.id=== a.id){
        this.cartItemList.splice(index,1);
      }
    })
    this.productList.next(this.cartItemList);
  }
  //Alle Produkte entfernen
  removeAllCart(){
    this.cartItemList = []
    this.productList.next(this.cartItemList);
  }
}
