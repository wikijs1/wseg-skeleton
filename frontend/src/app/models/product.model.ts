//Model Produkte
export class Product {
    id?: string;
    category?: string;
    productName?: string;
    productPrice?: string;
    productImage?: string;
    productDescription?: string;
    address?: string; 
    postalCode?: string;
    place?: string;
    quantity?: number;  
    total?: number;  
}
