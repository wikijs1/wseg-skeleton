import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models/product.model';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';
// import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})

export class ProductDetailsComponent implements OnInit, OnChanges  {

  @Input() product?: Product;
  @Output() refreshList: EventEmitter<any> = new EventEmitter();
  currentProduct: Product = {
    productName: '',
    productPrice: '',
    productImage: '', 
    productDescription: '',
    address: '', 
    postalCode: '', 
    place: ''
  };
  message = '';
  public products!: AngularFirestoreCollection<Product>;
  private sub : any;
  id: any;

  constructor(private productService: ProductService, private cartService: CartService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.message = '';
    //Bekommt von der URL die ID für das gewünschte Produkt
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    })
    this.products = this.productService.getAll()
    this.products.doc(this.id).ref.get()
    .then((doc) => {
      if (doc.exists){
        this.currentProduct = doc.data()!;    
        return doc.data();
      } else{
        return "Doc does not exist";
      }
      })
      .catch((err) => {
        console.error(err)
      });
  }

  ngOnChanges(): void {
    this.message = '';
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
    })

    this.products = this.productService.getAll()
    //Bekommt von der URL die ID für das gewünschte Produkt
    this.products.doc(this.id).ref.get()
    .then((doc) => {
      if (doc.exists){
        this.currentProduct = doc.data()!;   
        return doc.data();
      } else{
        return "Doc does not exist";
      }
      })
      .catch((err) => {
        console.error(err)
      });
  }
  

  addtocart(product: any){
    this.cartService.addtoCart(product);
  }

}
