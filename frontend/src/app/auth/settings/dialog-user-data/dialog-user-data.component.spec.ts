import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogUserDataComponent } from './dialog-user-data.component';

describe('DialogUserDataComponent', () => {
  let component: DialogUserDataComponent;
  let fixture: ComponentFixture<DialogUserDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DialogUserDataComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogUserDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
