import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Product } from '../models/product.model';
import { map } from 'rxjs/operators';
import { CartService } from '../services/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Variables & Arrays
  public productList : any ;
  products?: Product[];
  filterProducts?: Product[];
  productID: any; 
  productData: any; 
  selectedCategoryId = ''; 
  // productName: string; 

  constructor(
    private productService: ProductService, 
    private cartService: CartService, 
    private rout: Router) {  }

  ngOnInit(): void {
    //Gibt alle Produkte
    this.retrieveProducts("");
  }
  
  //Suche angefangen
  // search() {
  //   this.products = this.products?.filter(res=>{
  //     return res.productName?.toLocaleLowerCase().match(this.productName.toLocaleLowerCase());
  //   })
  // }

  //Gibt Produkte zurück
  retrieveProducts(category: any): void {
    this.productService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(data => {
      this.products = data.filter(c => c.category?.includes(category))
    });
  }

  //Fügt Produkt dem Warenkorb hinzu
  addtocart(product: any){
    this.cartService.addtoCart(product);
  }

  //Navigiert zu Produkt Details
  nav(i: any) {
    this.rout.navigate(['/details/' , i]); 
  }
  
  //Filtert die Kategorien von Produkten
  onCategoryChange(event: Event) {
    let selectedCategoryId = (event.target as HTMLSelectElement).value; 
    this.selectedCategoryId = selectedCategoryId;
    if (selectedCategoryId == '1') {
      console.log(this.products?.filter(c => c.category?.includes("1")))
      this.products =  this.products?.filter(c => c.category?.includes("1"))
    } else if (selectedCategoryId == '2') {
      console.log(this.products?.filter(c => c.category?.includes("2")))
      this.products =  this.products?.filter(c => c.category?.includes("2"))
    }else if (selectedCategoryId == '3') {
      console.log(this.products?.filter(c => c.category?.includes("3")))
      this.products =  this.products?.filter(c => c.category?.includes("3"))
    }else if (selectedCategoryId == '4') {
      console.log(this.products?.filter(c => c.category?.includes("4")))
      this.products =  this.products?.filter(c => c.category?.includes("4"))
    }else if (selectedCategoryId == '5') {
      console.log(this.products?.filter(c => c.category?.includes("5")))
      this.products =  this.products?.filter(c => c.category?.includes("5"))
    }else if (selectedCategoryId == '6') {
      console.log(this.products?.filter(c => c.category?.includes("6")))
      this.products =  this.products?.filter(c => c.category?.includes("6"))
    }
  }


}
