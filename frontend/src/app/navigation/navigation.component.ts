import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { CartService } from '../services/cart.service';
import { Product } from '../models/product.model';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
})
export class NavigationComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );
  
  //Erkennt ob Benutzer angemeldet ist
  get isAuthenticated() {
    return this.user !== null;
  }

  //Gibt Email zurück
  get email() {
    if (this.user) return this.user.email;
    return '';
  }

  user: firebase.default.User | null = null;

    // -----------------------
  // search
  public totalItem : number = 0;
  public searchTerm !: string;
  // --------------------------

  products?: Product[];
  selectedCategoryId = '';

  constructor(
    private auth: AngularFireAuth,
    private snackbar: MatSnackBar,
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private cartService: CartService,
    private productService: ProductService,
  ) {
    this.auth.authState.subscribe((user) => {
      this.user = user;
    });
  }

  //Gibt Produkte zurück
  retrieveProducts(category: any): void {
 
    this.productService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(data => {
      this.products = data.filter(c => c.category?.includes(category))
    });
  }

  ngOnInit(): void {
    this.retrieveProducts("");
    this.cartService.getProducts()
    .subscribe(res=>{
      this.totalItem = res.length;
    })
  }

  //Suche unvollständig
  search(event:any){
    this.searchTerm = (event.target as HTMLInputElement).value;
    console.log(this.searchTerm);
    this.cartService.search.next(this.searchTerm);
  }

  //Ausloggen
  logout() {
    this.auth.signOut().then(() => {
      this.snackbar.open('Bye!', 'Close', {
        duration: 2500,
      });
      this.router.navigate(['/home']);
    });
  }

  // onCategoryChange(event: Event) {
  //   let selectedCategoryId = (event.target as HTMLSelectElement).value; 
  //   this.selectedCategoryId = selectedCategoryId;
  //   if (selectedCategoryId == '1') {
  //     console.log(this.products?.filter(c => c.category?.includes("1")))
  //     this.products =  this.products?.filter(c => c.category?.includes("1"))
  //   } else if (selectedCategoryId == '2') {
  //     console.log(this.products?.filter(c => c.category?.includes("2")))
  //     this.products =  this.products?.filter(c => c.category?.includes("2"))
  //   }else if (selectedCategoryId == '3') {
  //     console.log(this.products?.filter(c => c.category?.includes("3")))
  //     this.products =  this.products?.filter(c => c.category?.includes("3"))
  //   }else if (selectedCategoryId == '4') {
  //     console.log(this.products?.filter(c => c.category?.includes("4")))
  //     this.products =  this.products?.filter(c => c.category?.includes("4"))
  //   }else if (selectedCategoryId == '5') {
  //     console.log(this.products?.filter(c => c.category?.includes("5")))
  //     this.products =  this.products?.filter(c => c.category?.includes("5"))
  //   }else if (selectedCategoryId == '6') {
  //     console.log(this.products?.filter(c => c.category?.includes("6")))
  //     this.products =  this.products?.filter(c => c.category?.includes("6"))
  //   }
  // }
}
