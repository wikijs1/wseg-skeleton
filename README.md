# WSEG Skeleton

This is a skeleton used for the practice projects in the WSEG course at the Bern University of Applied Sciences.

## Set up the Project

1. Each group member: Create a GitLab Account
2. Each group member: Login with your GitLab Account
3. Only ONCE per group: Fork this repository (top-right: click on "Fork")
4. Each group member: Clone your forked group repository locally in your development environment

## Set up Firebase

Only ONCE per group:

1. Sign up for firebase on https://firebase.google.com
2. Create a new project (you can disable Google Analytics)
3. Under Build / Authentication:
    1. Click "Get Started"
    2. Go to "Sign-in method"
    3. Add e-mail / password as new sign-in provider
4. Under Build / Hosting
    1. Click "Get Started"
    2. Follow the instructions
    3. Run ```firebase login:ci``` to get a Firebase Token
    4. Copy the token to the clipboard
5. In your GitLab project go to Settings / CI/CD
    1. Expand the section "Variables"
    2. Add a new Variable with the following properties
        - Key: FIREBASE_TOKEN
        - Value: paste the firebase token you copied to the clipboard in the previous step
        - Type: Variable
        - Environment Scope: All
        - Protect Variable: unchecked
        - Mask Variable: checked

## Run the project from your terminal

Each group member:

1. Make sure you have Node and npm installed (https://nodejs.org/)
2. Go to frontend (with ```cd frontend```)
3. Run ```npm install``` to install the dependencies
4. Run ```ng serve``` to start up the development server
5. Visit the site on http://localhost:4200/

Now you should be ready to go :)
