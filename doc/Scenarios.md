# Plattform iSell??

# Persona

## Hermine Granger
Hermine möchte gerne ihre neue Wohnung einrichten, will aber nicht zu viel Geld ausgeben für neue Möbel.
Sie will sich daher registrieren auf der iSell Plattform, um dort günstige und hochwertige Möbel zu erwerben.

## Albus Dumbledore
Albus möchte gerne seine alte Couch verkaufen, weil er sich eine neue gönnen möchte.
Daher will er sich auf der iSell Plattform registrieren, um dann dort eine Anzeige aufzugeben.

## Harry Potter
Harry ...

## Rubeus Hagrid
Rubeus ...


# Scenarios 
## Registrierung
Albus navigiert auf die Startseite der iSell Plattform und klickt dort auf den Knopf "Registrieren". 
Es öffnet sich eine Eingabemaske wo Albus nach seinem Vornamen, Namen, seiner E-Mail-Adresse und einem Passwort gefragt wird.
Nachdem er die Daten eingegeben hat, klickt er auf den Knopf "Profil erstellen".
Nachdem die Checks abgeschlossen wurden (korrekte E-Mail, E-Mail ist noch nicht registriert) wird sein Profil erstellt. 
Danach wird er aufgefordert, sein Profil zu bestätigen mit dem Link der auf seine E-Mail-Adresse gesendet wurde.

## Login
Albus hat erfolgreich ein Profil erstellt. 
Nun möchte er sich einloggen, um seine Anzeige zu schalten.
Er navigiert auf die Startseite der iSell Plattform und klickt dort auf den Knopf "Login".
Es öffnet sich eine Eingabemaske wo Albus nach seiner E-Mail-Adresse und dem Passwort gefragt wird.
Nachdem er die Daten eingegeben hat, klickt er auf den Knopf "Einloggen".
Nachdem die Checks abgeschlossen wurden (korrekte E-Mail, Passwort korrekt für E-Mail) wird er eingeloggt.

## Profil ändern
Albus hat sich erfolgreich eingeloggt.
Nun möchte er gerne sein Profil anpassen, um den Käufern etwas mehr über sich zu verraten.
Er klickt auf den Knopf "Profil".
Im Formular trägt er sein Geburtsdatum ein und schreibt eine kleine Beschreibung über sich.

## Passwort vergessen (optional)
Albus hat sein Passwort vergessen und kann sich somit nicht mehr einloggen.
Er drückt auf den Knopf "Passwort vergessen" und gelangt auf ein Formular wo er nach seiner E-Mail-Adresse gefragt wird.
Er gibt seine E-Mail ein und drückt auf den Knopf "Passwort zurücksetzen".
Albus geht zu seinem E-Mail-Client und klickt auf den Link, welchen er von der iSell Plattform erhalten hat.
Es öffnet sich ein Formular, wo er ein neues Passwort setzen kann.

