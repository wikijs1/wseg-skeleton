import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { SettingsComponent } from './auth/settings/settings.component';
import { HomeComponent } from './home/home.component';
import { AddProductComponent } from './add-product/add-product.component';
import { GetProductComponent } from './get-product/get-product.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { AboutComponent } from './about/about.component';
import { CartComponent } from './cart/cart.component';
import { AgmCoreModule } from '@agm/core';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'settings',
    component: SettingsComponent,
  },
  {
    path: 'add-product',
    component: AddProductComponent,
  },
  {
    path: 'product-details',
    component: ProductDetailsComponent,
  },
  {
    path: 'get-product',
    component: GetProductComponent,
  },
  {
    path: 'details/:id',
    component: ProductDetailsComponent,
    data: {title:'id'}
  },
  {
    path: 'edit-product',
    component: EditProductComponent,
  },
  {
    path: 'cart',
    component: CartComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), 
  AgmCoreModule.forRoot({
    apiKey: 'AIzaSyDmxlEkmkR9MPSp_Sh_Rs0ysePc8BIH-MQ', libraries : ['places']
  })
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
