import { NgIf } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Product } from '../models/product.model';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-get-product',
  templateUrl: './get-product.component.html',
  styleUrls: ['./get-product.component.css']
})
export class GetProductComponent implements OnInit {

  products?: Product[];
  currentProduct?: Product;
  currentIndex = -1;
  productName = '';


  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit(): void {
    this.retrieveProducts();
  }

  refreshList(): void {
    this.currentProduct = undefined;
    this.currentIndex = -1;
    this.retrieveProducts();
  }

  //Gibt Produkte zurück
  retrieveProducts(): void {
    this.productService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
      )
    ).subscribe(data => {
      this.products = data;
    });
  }

  setActiveProduct(product: Product, index: number): void {
    this.currentProduct = product;
    this.currentIndex = index;
  }

  hide: boolean = true; 
  toogleTag() {
    this.hide=!this.hide; 
  }

  
  // MORE INFO FUNCTION
  // productDetails(currentIndex) {
  //   this.router.navigate(['/get-product/details/' + currentIndex]);
  // }


  navigateToDetails =  () => {
    this.router.navigateByUrl('/product-details');
  }


}
