import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
//Service für Produkte
export class ProductService {
  productGetById(pId: string | null) {
    throw new Error('Method not implemented.');
  }

  private dbPath = '/products';

  productsRef: AngularFirestoreCollection<Product>;

  constructor(private db: AngularFirestore) {
    this.productsRef = db.collection(this.dbPath);
  }

  //Gibt alle Produkte zurück
  getAll(): AngularFirestoreCollection<Product> {
    return this.productsRef;
  }

  //Gibt einzelnes Produkt zurück
  getProduct(id: string): any {
    return this.productsRef.doc(id).ref.get()
    .then((doc) => {
      if (doc.exists){
        console.log(doc.data());
        return doc.data();
      } else{
        return "Doc does not exist";
      }
      })
      .catch((err) => {
        console.error(err)
      });

  }

  //Erstellt Produkt
  create(product: Product): any {
    return this.productsRef.add({ ...product });
  }

  //Update Produkt
  update(id: string, data: any): Promise<void> {
    return this.productsRef.doc(id).update(data);
  }

  //Löscht Produkt
  delete(id: string): Promise<void> {
    return this.productsRef.doc(id).delete();
  }

}
