//Interface für Benutzer
export interface UserData {
  name: string;
  surname: string;
  description: string;
}
