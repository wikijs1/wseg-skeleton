import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit, OnChanges {

  @Input() product?: Product;
  @Output() refreshList: EventEmitter<any> = new EventEmitter();
  currentProduct: Product = {
    category: '',
    productName: '',
    productPrice: '',
    // quantity: number,
    productImage: '',
    productDescription: '',
    address: '', 
    postalCode: '', 
    place: '',
  };
  message = '';

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.message = '';
  }

  ngOnChanges(): void {
    this.message = '';
    this.currentProduct = { ...this.product };
  }

  //Produkt updaten
  updateProduct(): void {
    const data = {
      category: this.currentProduct.category,
      productName: this.currentProduct.productName,
      productPrice: this.currentProduct.productPrice,
      quantity: this.currentProduct.quantity,
      productImage: this.currentProduct.productImage,
      productDescription: this.currentProduct.productDescription,
      address: this.currentProduct.address, 
      postalCode: this.currentProduct.postalCode, 
      place: this.currentProduct.place,  
    };

    if (this.currentProduct.id) {
      this.productService.update(this.currentProduct.id, data)
        .then(() => this.message = 'The Product was updated successfully!')
        .catch(err => console.log(err));
    }
    console.log(this.currentProduct.id)
  }

  //Produkt löschen
  deleteProduct(): void {
    if (this.currentProduct.id) {
      this.productService.delete(this.currentProduct.id)
        .then(() => {
          this.refreshList.emit();
          this.message = 'The Product was updated successfully!';
        })
        .catch(err => console.log(err));
    }
  }

}
