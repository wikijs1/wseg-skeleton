import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { MatDialog } from '@angular/material/dialog';
import { UserData } from 'src/app/user-data';
import { DialogUserDataComponent } from './dialog-user-data/dialog-user-data.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css'],
})
export class SettingsComponent implements OnInit {
  private userId: string | undefined;

  private userdataDoc: AngularFirestoreDocument<UserData> | undefined;

  userdata: UserData | undefined;

  constructor(
    private auth: AngularFireAuth,
    private afs: AngularFirestore,
    private dialog: MatDialog
  ) {
    this.auth.authState.subscribe((user) => {
      if (!user) return;
      console.log(user.uid);
      this.userId = user.uid;
      this.subscribeToUserdata();
    });
  }

  ngOnInit(): void {}

  createUserdata() {
    const newUserdata: UserData = {
      description: '',
      name: '',
      surname: '',
    };
    this.dialog
      .open(DialogUserDataComponent, {
        width: '50%',
        data: newUserdata,
      })
      .afterClosed()
      .subscribe((userdata) => {
        console.log('new data', userdata);
        if (!this.userId) throw new Error('Expected to have a user id');
        this.afs
          .collection('user-data')
          .doc(this.userId)
          .set(userdata)
          .then(() => {
            this.subscribeToUserdata();
          });
      });
  }

  updateUserdata() {
    this.dialog
      .open(DialogUserDataComponent, {
        width: '50%',
        data: this.userdata,
      })
      .afterClosed()
      .subscribe((userdata) => {
        console.log('updated data', userdata);
      });
  }

  deleteUserdata() {
    if (this.userdataDoc) {
      console.log('Deleting');
      this.userdataDoc.delete();
    }
  }

  private subscribeToUserdata() {
    this.userdataDoc = this.afs.doc<UserData>(`user-data/${this.userId}`);
    this.userdataDoc.valueChanges().subscribe((userdata) => {
      this.userdata = userdata;
    });
  }
}
