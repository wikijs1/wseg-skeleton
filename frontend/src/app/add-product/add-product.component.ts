import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from '../models/product.model';
import { ProductService } from '../services/product.service';
import { AngularFireStorage, AngularFireStorageReference, AngularFireUploadTask } from '@angular/fire/storage';
import { map, finalize } from "rxjs/operators";
import { Observable } from "rxjs";
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';


@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit{

  product: Product = new Product();
  submitted = false;
  selectedFile!: File;
  fb!: string;
  downloadURL!: Observable<string>;

  @ViewChild("placesRef") placesRef!: GooglePlaceDirective;

  // options = {
  //   type : [], 
  //   componentRestrictions: { country: 'CH'}
  // }
  // title_add: any;  
  // latitude: any; 
  // longitude: any; 
  // zoom: any; 


  constructor(private storage: AngularFireStorage, private productService: ProductService) { }

  ngOnInit() {
    // this.setCurrentLocation()
  }

  // public handleAddressChange(address: Address) {
  //   // Do some stuff
  //   console.log(address)
  //   console.log('Latitud : ' + address.geometry.location.lat())
  //   console.log('Longitud : ' + address.geometry.location.lng())

  //   this.latitude = address.geometry.location.lat(); 
  //   this.longitude = address.geometry.location.lng();
  // }

  // public setCurrentLocation() {
  //   if ('geolocation' in navigator) {
  //     navigator.geolocation.getCurrentPosition((position) => {
  //       this.latitude = position.coords.latitude; 
  //       this.longitude = position.coords.longitude; 
  //       this.zoom = 15; 

  //       console.log('Latitud : ' + this.latitude)
  //       console.log('Longitud : ' + this.longitude)
  //     })
  //   }
  // }

  //Hochladen eines Bild
  onFileSelected(event: any) {
    var n = Date.now();
    const file = event.target.files[0];
    const filePath = `ProductImages/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`ProductImages/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();

          this.downloadURL.subscribe(url => {
            if (url) {
              this.fb = url;
              this.product.productImage = url
            }
            console.log(this.fb);
          });
        })
      )
      .subscribe(url => {
        if (url) {
          console.log(url);
        }
      });
  }

  //Artikel speichern
  saveProduct(): void {
    this.productService.create(this.product).then(() => {
      console.log('Created new item successfully!');
      this.submitted = true;
    });
  }

  //Neuer Artikel
  newProduct(): void {
    this.submitted = false;
    this.product = new Product();
  }


}
