import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public products : any = [];
  public grandTotal !: number;

  constructor(private cartService: CartService) { }

  
  ngOnInit(): void {
    //Service übergibt Warenkorb
    this.cartService.getProducts()
    .subscribe(res=>{
      this.products = res;
      this.grandTotal = this.cartService.getTotalPrice();
    })
    this.grandTotal = this.cartService.getTotalPrice();
  }

  //Entfernt Artikel aus dem Warenkorb
  removeItem(product: any){
    this.cartService.removeCartItem(product);
  }

  //Entfernt alle Artikel aus dem Warenkorb
  emptycart(){
    this.cartService.removeAllCart();
  }

}
