// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDmxlEkmkR9MPSp_Sh_Rs0ysePc8BIH-MQ",
    authDomain: "wseg-shop.firebaseapp.com",
    databaseURL: "https://wseg-shop-default-rtdb.firebaseio.com",
    projectId: "wseg-shop",
    storageBucket: "wseg-shop.appspot.com",
    messagingSenderId: "857242682992",
    appId: "1:857242682992:web:f0d07ff8e8deab7ac4d6b5",
    //measurementId: "G-GQT0CBTTGD"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
