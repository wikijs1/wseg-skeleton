import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserData } from 'src/app/user-data';

@Component({
  selector: 'dialog-user-data',
  templateUrl: './dialog-user-data.component.html',
  styleUrls: ['./dialog-user-data.component.css'],
})
export class DialogUserDataComponent implements OnInit {
  userdataForm = this.fb.group({
    name: [this.userdata.name, Validators.required],
    surname: [this.userdata.surname],
    description: [this.userdata.description],
  });

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<DialogUserDataComponent>,
    @Inject(MAT_DIALOG_DATA) public userdata: UserData
  ) {
    this.dialogRef.beforeClosed().subscribe(() => {
      this.submitUserdata();
    });
  }

  ngOnInit(): void {}

  isFormValid() {
    return this.userdataForm.valid;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  private submitUserdata() {
    this.userdata.name = this.userdataForm.value.name;
    this.userdata.surname = this.userdataForm.value.surname;
    this.userdata.description = this.userdataForm.value.description;
  }
}
